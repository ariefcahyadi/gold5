/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
    /* Your site config here */
    siteMetadata: {
        siteUrl: `https://www.undanganku.me`,
    },
    plugins: [`gatsby-plugin-sass`, {
            resolve: `gatsby-plugin-facebook-pixel`,
            options: {
                pixelId: "669707900211107",
            },
        },
        {
            resolve: `gatsby-plugin-react-helmet`,

        },
        {
            resolve: `gatsby-plugin-disqus`,
            options: {
                shortname: `https-undanganku-me`
            }
        },
        {
            resolve: `gatsby-plugin-advanced-sitemap`,
            options: {

                exclude: [
                    `/dev-404-page`,
                    `/404`,
                    `/404.html`,
                    `/offline-plugin-app-shell-fallback`,
                    `/my-excluded-page`,
                    /(\/)?hash-\S*/, // you can also pass valid RegExp to exclude internal tags for example
                ],
                createLinkInHead: true, // optional: create a link in the `<head>` of your site
                addUncaughtPages: true, // optional: will fill up pages that are not caught by queries and mapping and list them under `sitemap-pages.xml`
                additionalSitemaps: [ // optional: add additional sitemaps, which are e. g. generated somewhere else, but need to be indexed for this domain
                    {
                        name: `Blog Post`,
                        url: `/blog/sitemap-posts.xml`,
                    },
                    {
                        url: `https://undanganku.me/sitemap.xml`,
                    },
                ],
            }
        },
        {
            resolve: `gatsby-plugin-google-gtag`,
            options: {
                // You can add multiple tracking ids and a pageview event will be fired for all of them.
                trackingIds: [
                    "G-2RZSCJM4RF", // Google Analytics / GA
                    // "AW-CONVERSION_ID", // Google Ads / Adwords / AW
                    // "DC-FLOODIGHT_ID", // Marketing Platform advertising products (Display & Video 360, Search Ads 360, and Campaign Manager)
                ],
            },
        },

    ]
}