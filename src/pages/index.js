import React, { Component } from 'react'
import Container from 'react-bootstrap/Container'
import { Item } from '../components/main'
import { Helm } from '../components/header'
import { Row, Col } from 'react-bootstrap'
import '../style/stylehomepage.css'
import gambar1 from '../assets/img/homepage/gambar1.png'
import gambar2 from '../assets/img/homepage/gambar2.png'
import silver1 from '../assets/img/homepage/silver1.jpg'
import silver2 from '../assets/img/homepage/silver2.jpg'
import {pw} from '../params'
import logo from '../assets/img/logo.ico'
import { gambar } from '../params'

import 'bootstrap/dist/css/bootstrap.min.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from "@fortawesome/fontawesome-svg-core";
import { faShoppingCart, faPhotoVideo, faMoneyBillAlt, faMusic, faClock, faMapMarkedAlt } from "@fortawesome/free-solid-svg-icons";
library.add(faShoppingCart, faPhotoVideo, faMoneyBillAlt, faMusic, faClock, faMapMarkedAlt);


export default class Halo extends Component {
  constructor(props) {
    super(props);
    this.catalog = React.createRef()
    this.state = {
      catalog1: false
    }
  }
  useQuery() {
    return new URLSearchParams(window.location.search);
  }


  componentDidMount() {
    window.location.href='https://undang.in'
    let query = this.useQuery().get('to');
    if (query) {
      setTimeout(() => {
        var elmnt = document.getElementById(query);
        elmnt.scrollIntoView();
      }, 1000)
    } else {
      setTimeout(() => {
        var elmnt = document.getElementById('home');
        elmnt.scrollIntoView();
      }, 1000)
    }

  }
  toWa = () => {
    window.open('https://api.whatsapp.com/send?phone=+6285156454015&text=Halo+Admin%2C+mau+tanya+Undangan+Digital+Dong%21',
      '_blank')
  }

  render() {

    let fitur = [
      [faPhotoVideo,
        'Bagikan momen bahagia berupa foto atau video kedalam undangan',
      ],
      [faMapMarkedAlt,
        'Fitur Google Maps akan membantu tamu menemukan lokasi pernikahan kamu',
      ],
      [faClock,
        'Fitur Penghitung Mundur Waktu agar tamu undangan tau hari pernikahan kamu.',
      ],
      [faMusic,
        'Punya lagu favorit? Masukan kedalam Undangan agar lebih berkesan',
      ],
      [faMoneyBillAlt,
        'Harga Terjangkau! Kamu bisa dapetin Undangan Mulai 100 Ribuan aja!',
      ]
    ]
    let catalog = [
      {
        type: 'platinum',
        image: gambar(pw('asset/preview-catalog',"cp1.jpg"), 95),
        url: 'https://ainipanji.com'
      },
      {
        type: 'platinum',
        image: gambar(pw('asset/preview-catalog',"cp2.jpg"), 95),
        url: 'https://herlina-enggar.com'
      },
      {
        type: 'gold',
        image: gambar('https://i.ibb.co/Pmcnxj3/Catalog-Gold-1.jpg', 95),
        url: '/dinny-azis?x=x&name=contoh+nama'
      },
      {
        type: 'gold',
        image: gambar('https://i.ibb.co/XC16xqf/Catalog-Gold-2.jpg', 95),
        url: '/fitri-ralo?x=x&name=contoh+nama'
      },
      {
        type: 'gold',
        image: gambar('https://i.ibb.co/rM79X5C/Catalog-Gold-3.jpg', 95),
        url: '/kezia-bram?x=x&name=contoh+nama'
      },
      {
        type: 'gold',
        image: gambar('https://i.ibb.co/QN3KqPF/Catalog-Gold-4.jpg', 95),
        url: '/aulia-yossa?x=x&name=contoh+nama'
      },
      {
        type: 'gold',
        image: gambar('https://i.ibb.co/5hD29bY/Catalog-Gold-5.jpg', 95),
        url: '/mutia-raihan?x=x&u=contoh+nama'
      },
      {
        type: 'gold',
        image: gambar('https://i.ibb.co/Nm4RY64/Catalog-Gold-6.jpg', 95),
        url: '/nasta-jojo?x=x&name=contoh+nama'
      },
      {
        type: 'gold',
        image: gambar('https://i.ibb.co/J3mG3qJ/Catalog-Gold-7.jpg', 95),
        url: '/gold3?x=x&name=contoh+nama'
      },
      {
        type: 'gold',
        image: gambar('https://i.ibb.co/MShBhT9/Catalog-Gold-8.jpg', 95),
        url: '/gold4?x=x&name=contoh+nama'
      },
      {
        type: 'gold',
        image: gambar('https://i.ibb.co/C23V8gn/Catalog-Gold-9.jpg', 95),
        url: '/gold5?x=x&name=contoh+nama'
      },
      {
        type: 'gold',
        image: gambar('https://i.ibb.co/fG2gKwL/Catalog-Gold-10.jpg', 95),
        url: '/gold2?x=x&name=contoh+nama'
      },
      {
        type: 'silver',
        image: silver1,
        url: '/arif-devia'
      },
      {
        type: 'silver',
        image: silver2,
        url: 'http://possiblewedding.online/grace-matias/'
      },
      {
        type: 'silver',
        image: gambar('https://i.ibb.co/Xjw3zdf/Catalog-Silver-3.jpg', 95),
        url: '/tuti-hendra?x=x&name=contoh+nama'
      },
      {
        type: 'silver',
        image: gambar('https://i.ibb.co/chh10mL/Catalog-Silver-4.jpg', 95),
        url: '/dea-dayat?x=x&name=contoh+nama'
      },
      {
        type: 'silver',
        image: gambar('https://i.ibb.co/P6qH5nm/Catalog-Silver-5.jpg', 95),
        url: '/nanda-yogi?x=x&name=contoh+nama'
      },
      {
        type: 'silver',
        image: gambar(pw('asset/preview-catalog',"cs6.jpg"), 95),
        url: '/wida-ismail?x=x&u=contoh+nama'
      },

    ]

    return (
      <>
        <Helm
          title='Undanganku - Undangan Online Berbasis Website'
          desc="Undangan berbasis online harga mulai 100 ribuan, responsive, interaktif, dan modern"
          logo={logo}
        />

        <div id='home'>
  
          <div className='w-100' style={{
            overflow: 'hidden', maxWidth: '100vw'
          }}>
            <div className="jumbotron jumbotron-fluid mb-0 w-100" id='section1' style={{
              overflow: 'hidden', maxWidth: '100vw'
            }}>
              <div className="container" >
                <div className='position' >
                  <div className='justify-content-md-start row justify-content-center'>
                    <h2 className='text-center text-lg-left'>
                      <div className='d-block d-md-inline'> Undangan Digital </div>
                      <div className='d-block d-lg-inline'>Berbasis Website</div>
                    </h2>
                  </div>

                </div>
                <div className='justify-content-md-start row justify-content-center row'>

                  <h1
                    className='mt-3 p-3 p-md-0 text-center text-lg-left'

                  >
                    Bikin Momen Pernikahanmu Lebih Berkesan Dengan Undangan Digital
                </h1>
                </div>
                <div className='justify-content-md-start row justify-content-center row'>

                  <p className='text-center text-lg-left'>
                    Sebar Undangan ke Penjuru Dunia tanpa batasan. Mulai 100Ribuan aja!
                      </p>
                </div>

                <div className='justify-content-md-start row justify-content-center row m-0 mt-md-5 p-0 '
                >
                  <Col xs={8} md={4} className='button ml-0 pt-md-2 pb-md-2 btn'>
                    <Item>
                      <div onClick={() => {
                        window.scrollTo({
                          top: this.catalog.current.offsetTop,
                          behavior: 'smooth'
                        })
                      }} className="mt-auto mb-auto">Cek Catalog</div></Item>
                  </Col>
                </div>
              </div>
            </div>
            <Container id='section2' className='w-100' >
              <div className='main'>
                <Item>
                  <Col xs={12} md={4} >
                    <h1 className='mt-md-auto mb-md-auto ' >
                      UNDANGANKU, ADA BUAT KAMU!
                  </h1>
                  </Col>

                  <Col xs={12} md={8} >
                    <p className='d-block'>
                      Kami adalah penyedia jasa pembuatan Undangan Digital Berbasis Website dengan berbagai
                      fitur menarik dan futuristik. Undangan Digital punya banyak kelebihan, salah satunya
                      bisa disebarkan secara instant tanpa batasan hanya dengan mengirimkan Link.Bikin momen
                      spesialmu makin berkesan dengan Undangan Digital berbasis website!
                    </p>
                    <p className='d-block'>
                      Bikin momen spesialmu makin berkesan dengan Undangan Digital berbasis website!
                    </p>
                  </Col>
                </Item>
              </div>
            </Container>
            <Container id='section3'>
              <Row>
                <Col xs={12} md={6}>
                  <img src={gambar1} className='img-fluid w-100' />
                </Col>
                <Col xs={12} md={6} >
                  <p>
                    Setiap Undangan dibuat oleh para ahli yang berpengalaman dalam bidang Desain. Karena
                    kami paham, setiap orang ingin yang terbaik di momen bahagianya.
                </p>
                </Col>
              </Row>
            </Container>
            <Container id='section4'>
              <Row>
                <Col xs={12}>
                  <h1 className='d-block text-center' >
                    Apa Saja Fiturnya?
                  </h1>
                </Col>
                <Item>
                  {fitur.map((val, i) => {

                    return (
                      <Col xs={12} md={2} className='kotak ml-md-2 mr-md-2' >
                        <Item>
                          <Col xs={4} md={8}>
                            <FontAwesomeIcon icon={val[0]} className='mr-2 mt-auto mb-auto img-fluid w-100' color='#F2BC13' />
                          </Col>
                          <p>
                            {val[1]}
                          </p>

                        </Item>
                      </Col>
                    )
                  })}
                </Item>
                <Col xs={8} md={4} className='button ml-0 pt-md-2 pb-md-2 mr-auto ml-auto btn' >
                  <Item>
                    <FontAwesomeIcon icon={faShoppingCart} className='mr-2 mt-auto mb-auto' />
                    <div onClick={() => {
                      this.toWa()
                    }}>Pesan Sekarang</div>
                  </Item>
                </Col>
              </Row>

            </Container>

            <Container id='section5' fluid>
              <Row className='mt-md-5'>
                <Col xs={12} md={6}>
                  <img src={gambar2} className='img-fluid w-100' />
                </Col>
                <Col xs={12} md={6} className='pl-md-5'>
                  <div className='pt-md-5'>
                    <Row>
                      <p >
                        Penasaran? Yuk cek Catalog Designnya!
                  </p>
                    </Row>
                    <Row className='justify-content-center justify-content-md-start'>
                      <div className='button col-8 text-center btn'

                        onClick={
                          () => {
                            window.scrollTo({
                              top: this.catalog.current.offsetTop,
                              behavior: 'smooth'
                            })
                          }
                        }>
                        Cek Katalog
                    </div>
                    </Row>
                  </div>
                </Col>
              </Row>
            </Container>

            <Container id='catalog' ref={this.catalog} fluid>
              <h1 id='catalog1'>
                Our <span>Catalog</span>
              </h1>
              <Row>
                {
                  catalog.map(val => {
                    return (
                      <Col xs={12} md={6} relative className='mb-3' >
                        <img src={val.image} className='img-fluid w-100 rounded' />
                        <div className='row w-100 h-100 kotak ml-auto mr-auto' >
                          <Col xs={6} className='mt-auto w-100'>
                            <Col xs={12} className='button btn' onClick={() => {
                              window.open(`${val.url}`, '_blank')
                            }}>Preview</Col>
                            <Col xs={12} className='button order btn' onClick={() => {
                              window.open(`https://api.whatsapp.com/send?phone=+6285156454015&text=Halo+Admin%2C+mau+tanya+Undangan+Digital+Dong%21+template+${val.url}`, '_blank')
                            }}>Order Now</Col>
                          </Col>
                        </div>
                      </Col>
                    )
                  })
                }

              </Row>
            </Container>

          </div>
        </div>
      </>


    )
  }
}
