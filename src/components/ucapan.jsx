
import { Form, Row, Col, Alert, Container } from 'react-bootstrap'
import React, { Component } from 'react'
import Header, { Item, Float, Foot, Slider } from '../components/main'
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css';
import post from '../params/post'


export default class Ucapan extends Component {
    constructor(props) {
        super(props);
        this.myRef = React.createRef()
        this.myRef = React.createRef()
        this.nama = React.createRef()
        this.alamat = React.createRef()
        this.pesan = React.createRef()
        this.state = {
            hadir: true,
            err: [],
            submitted: '',
            sesi: 1
        }
    }
    handleSubmit = async () => {
        let {id} = this.props
        let err = []
        let local = localStorage.getItem('pesan')
        if (this.nama.current.value == "") {
            err.push('Nama tidak Boleh Kosong')
        }
        if (this.pesan.current.value == "") {
            err.push('Pesan tidak Boleh Kosong')
        }
        if (err.length == 0) {
            confirmAlert({
                message: local ? `Kami mendeteksi bahwa anda telah mengirimkan pesan \" ${local} \", apakah anda ingin mengirim pesan lagi?` : 'Yakin untuk Mengirim Pesan?',
                buttons: [
                    {
                        label: 'Yes',
                        onClick: async () => {
                            let send = await post(
                                ` dari: "${this.nama.current.value}", hadir: "${this.state.hadir}${this.state.hadir?"sesi"+this.state.sesi:''}", jumlahhadir: "", kepada: "${id}", pesan:"${this.pesan.current.value}",alamat: ""`
                            )
                            if (send.status == 200) {
                                this.setState({ submitted: true })
                                localStorage.setItem('pesan', this.pesan.current.value)
                                this.nama.current.value = ''
                                this.pesan.current.value = ''
                            }else{
                                err.push('Koneksi Gagal')
                            }
                        }
                    },
                    {
                        label: 'No',
                        onClick: () => { }
                    }
                ]
            });
        } else {
            this.setState({ err: err, submitted: false })
        }
    }
    render() {
        let { cmain } = this.props
        let { submitted, err,sesi,hadir } = this.state
        return (<>
            <Container id='sectiongold58' >

                <div className='pt-3 mt-4 mt-lg-5 mb-lg-3'>
                    
                    <Item>
                        <div className='col-10 col-lg-6 kotak pb-4 pt-4' data-aos="left-left" data-aos-duration="1000">
                            <Item>
                                <h1 style={{
                                    fontFamily: '"Marck Script", cursive',
                                    color: 'rgb(185, 146, 37)'
                                }}>
                                    Send Your Wishes
                                </h1>
                            </Item>
                            <Item>
                                <form className="col-12 w-100">
                                    <input ref={this.nama} type='text' className="col-12 w-100 text-center" placeholder="Name" name='nama' />
                                    <input ref={this.pesan} type='text-area' className="col-12 w-100 text-center bigger" placeholder="Message" name='pesan' />
                                    <Item>
                                        <div id="formradio">
                                            <div class="custom_radio row justify-content-center">
                                                <div onClick={() => {
                                                    this.setState({ hadir: true })
                                                }
                                                }>
                                                    <input type="radio" id="featured-1" name="featured" checked={hadir ? true : false} />
                                                    <label for="featured-1">Hadir</label>
                                                </div>
                                                <div onClick={() => {
                                                    this.setState({ hadir: false })
                                                }
                                                } className="pl-5">
                                                    <input type="radio" id="featured-2" name="featured" checked={hadir ? false : true} />
                                                    <label for="featured-2"

                                                    >Tidak Hadir</label>
                                                </div>
                                                {!hadir ? false : (
                                                    <>  <Alert variant='dark col-12 mr-4 '>
                                                        <p className="f-14">
                                                            Silahkan Pilih Jadwal Kedatangan
                    </p>
                                                        <div onClick={() => {
                                                            this.setState({ sesi: 1 })
                                                        }
                                                        }
                                                            className="pl-5">
                                                            <input type="radio" id="featured-3" name="sesi" checked={sesi == 1 ? true : false} />
                                                            <label for="featured-3">

                                                                Sesi 1 : Akad Nikah</label>
                                                        </div>
                                                        <div onClick={() => {
                                                            this.setState({ sesi: 2 })
                                                        }
                                                        } className="pl-5">
                                                            <input type="radio" id="featured-4" name="sesi" checked={sesi == 2 ? true : false} />
                                                            <label for="featured-4"

                                                            >Sesi 2 : Resepsi</label>
                                                        </div>
                                                        <p className="f-14">
                                                            Tamu undangan diharapkan hadir sesuai dengan sesi yang telah ditentukan

                    </p>
                                                    </Alert>



                                                    </>

                                                )}

                                            </div>
                                        </div>
                                    </Item>

                                    <Item>
                                        <Col xs={12} className=''>
                                            {
                                                submitted == true ? (
                                                    <Alert variant='success' style={{ fontSize: '16px' }}>
                                                        Pesan anda sudah disampaikan
                                                    </Alert>) : (submitted === false ? (
                                                        <Alert variant='danger' style={{ fontSize: '16px' }}>
                                                            {
                                                                err.map(val => {
                                                                    return (
                                                                        <li>{val}</li>
                                                                    )
                                                                })
                                                            }

                                                        </Alert>
                                                    ) : false)
                                            }

                                        </Col>
                                    </Item>
                                    <Item>
                                        <div className='col-6 button rounded btn'
                                            onClick={() => this.handleSubmit()} style={{ backgroundColor: cmain }} no> Kirim </div>
                                    </Item>
                                </form>
                            </Item>
                        </div>
                    </Item>
                </div>
            </Container>

        </>)
    }

}