
import React, { Component } from 'react'
import { Table } from 'reactstrap';
import { CSVReader } from 'react-papaparse'
import Container from 'react-bootstrap/Container'
import { gambar } from '../params'
import { Form, Row, Col } from 'react-bootstrap'
import Alert from 'react-bootstrap/Alert'
import '../style/style.css'
import bunga from '../assets/img/bunga.svg'

import bungaFlip from '../assets/img/bunga-flip.svg'
import head from '../assets/img/modal2.jpg'
import s1 from '../assets/img/s1.jpg'
import s2 from '../assets/img/s2.jpg'
import s3 from '../assets/img/s3.jpg'
import s4 from '../assets/img/s4.jpg'
import AOS from 'aos';
import "aos/dist/aos.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowCircleDown } from '@fortawesome/free-solid-svg-icons'
import AwesomeSlider from 'react-awesome-slider';
import withAutoplay from 'react-awesome-slider/dist/autoplay';
import 'react-awesome-slider/dist/styles.css';
const AutoplaySlider = withAutoplay(AwesomeSlider);


const Header = (props) => {
        return (<>
                <Container fluid style={{ paddingLeft: 0, paddingRight: 0, width: '100vw', overflow: 'hidden' }} id='header'>
                        <div id='header' className='d-flex '>
                                <div style={{
                                        overflow: 'hidden',
                                }} data-aos="fade-right"
                                        data-aos-duration="1200">
                                        <img
                                                src={bungaFlip}
                                                style={{
                                                        marginTop: '-10vh',
                                                        marginLeft: '-14vh',
                                                        height: '40vh'
                                                }} /></div>
                                <div className='ml-auto'
                                        style={{
                                                overflow: 'hidden'
                                        }} data-aos="fade-left"
                                        data-aos-duration="1200">
                                        <img
                                                src={bunga}
                                                style={{
                                                        height: '40vh',
                                                        marginRight: '-14vh',
                                                        marginTop: '-10vh'
                                                }} />
                                </div>
                        </div>
                        <div>
                                <Row data-aos="fade-in"
                                        data-aos-duration="1200"
                                        className='gb text-center justify-content-center'>
                                        <Col md={6} xs={6}
                                                className='gborder p-md-3 p-1 '
                                        >
                                                <Row className='wedding justify-content-center '
                                                >
                                                        <p className='f-small'>The Wedding of</p>
                                                </Row>
                                                <Row className='wedding justify-content-center '
                                                ><div>
                                                                <span>
                                                                        Ririn
                                                </span>
                                                                <span className='ml-2 mr-2 d-block'>
                                                                        &
                                                </span>
                                                                <span>
                                                                        Rozaq
                                                </span>
                                                        </div>
                                                </Row>
                                                <Row className='wedding justify-content-center '
                                                >
                                                        <p className='f-small pt-3'>
                                                                07 November 2020</p>
                                                </Row>

                                        </Col>
                                </Row>
                        </div>
                        <div>
                                <Row className=' text-center gambarutama'
                                        data-aos="fade-down"
                                        data-aos-duration="1200">
                                        <Col >
                                                <img src={gambar('https://i.ibb.co/WKWJqCH/MG-9656-2.jpg', 95,
                                                        'auto&func=fit&bg_img_fit=1&bg_opacity=0.75&w=1200&h=900')}
                                                />
                                        </Col>
                                </Row>
                        </div>

                        <div className='tombolNext' data-aos="fade-up"
                                data-aos-duration="1200"
                                data-aos-delay="300"
                        ></div>
                </Container>
        </>
        )
}
export const Item = (props) => {
        return (
                <Row className='justify-content-center'
                >
                        {props.children}
                </Row>
        )
}
export const Mempelai = (props) => {
        return (
                <>
                        <Item>
                                <p style={
                                        {
                                                fontFamily: 'Sacramento, cursive',
                                                fontSize: '2rem',
                                                lineHeight: '2'
                                        }
                                }>
                                        {props.children}
                                </p>
                        </Item>
                </>
        )
}
export const Divider = (props) => {
        return (
                <>
                        <div
                                style={{ position: 'absolute', bottom: '0', left: '0' }}>
                                <img src={props.img} style={{ minWidth: '100%', minHeight: '100%' }} />
                        </div>
                </>
        )
}
export const Slider = (props) => {
        let def = [s1, s2, s3, s4]

        return (
                <>
                        <AutoplaySlider
                                play={true}
                                cancelOnInteraction={false} // should stop playing on user interaction
                                interval={2000}
                        >
                                {
                                        props.slide ?
                                                props.slide.map(val => {
                                                        return <div data-src={val} />
                                                })
                                                :
                                                def.map(val => {
                                                        return <div data-src={val} />
                                                })

                                }

                        </AutoplaySlider>
                </>
        )
}

export const SliderA = (props) => {
        let def = [s1, s2, s3, s4]

        return (
                <>
                        <AutoplaySlider
                                play={true}
                                cancelOnInteraction={false} // should stop playing on user interaction
                                interval={2000}
                        >
                                {
                                        props.slide ?
                                                props.slide.map(val => {
                                                        return <div data-src={val} />
                                                })
                                                :
                                                def.map(val => {
                                                        return <div data-src={val} />
                                                })

                                }

                        </AutoplaySlider>
                </>
        )
}

export default Header


export const Foot = (props) => {
        return (
                <Container className='text-center py-5' id='sectiongold59'>
                        <Item>
                                <Col xs={10} md={5}>
                                        <img className="img-fluid w-100 btn p-0"
                                                src={props.dark ? 'https://i.ibb.co/PZSL7g7/Watermark-Dark.png' : "https://i.ibb.co/GMwkwyw/Watermark-Light.png"} />
                                </Col>
                        </Item>
                        <Item>
                                <img src={props.ig} width="35px" height="35px"
                                        onClick={() => { window.open('https://instagram.com/possiblewedding') }}
                                        className="btn p-0"
                                />
                        </Item>
                </Container>
        )
}

export const Float = (props) => {
        return (
                // <div className="col-12 fixed-top">
                //         <Row className="p-3 px-5 justify-content-center" style={{ backgroundColor: '#F2BC13' }}>
                //                 <Col xs={12} md={4} className='btn' onClick={() => {
                //                         window.open(`http://bit.ly/PossibleWedding`, '_blank')
                //                 }}
                //                         style={{ backgroundColor: 'white', color: 'black', borderRadius: '15px' }}
                //                 >Order Now</Col>
                //         </Row>
                // </div>
                <>
                </>
        )
}