import React, { Component } from 'react'
import { Table } from 'reactstrap';
import Container from 'react-bootstrap/Container'
import { Form, Row, Col, Button } from 'react-bootstrap'
import "aos/dist/aos.css";
import 'bootstrap/dist/css/bootstrap.min.css';


export const Image =(props)=>{
    return(<>
        <Row className='justify-content-center'>
                    <Col md={6} xs={8} className='p-1'>
                      <img className='img-fluid rounded' src={props.src}/>
                    </Col>
                    </Row>
                    </>
    )
}
