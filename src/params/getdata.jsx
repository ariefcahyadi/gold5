import axios from 'axios'

const getData=async(data)=>{
    
       let res=await axios({
            url: 'https://api-ap-northeast-1.graphcms.com/v2/ckibrbrhpi9j701xth4upbvyj/master',
            method: 'post',
            data: {
              query: `
              query MyQuery {
                undangans(where: {${data}}, stage: DRAFT,first:1000) {
                  id
                  jumlahhadir
                  kepada
                  pesan
                  dari
                  alamat
                  hadir
                }
              }
              
                `
            }
          })
          return res.data.data.undangans
}

export default getData